const express = require('express');
const { randomBytes } = require('crypto');
const bodyParser = require('body-parser');
const axios = require('axios');

const app = express();
app.use(bodyParser.json());

// prevent cors error
const cors = require('cors');
app.use(cors());

const posts = {};

app.get('/posts', (req, res) => {
  res.send(posts);
});

app.post('/posts', (req, res) => {
  const id = randomBytes(4).toString('hex');
  const title = req.body;
  
  posts[id] = { id, title };

  axios.post( 'http://localhost:4005/events', {
    type: 'PostCreated',
    data: {
      id, title
    }
  });
  
  res.status(201).send(id);
});

// Responding the Event Bus event.
app.post('/events', (req, res) => {
  console.log('Received Event:', req.body.type);
  res.send({});
});

app.listen(4000, () => {
  console.log('Listening on 4000');
});